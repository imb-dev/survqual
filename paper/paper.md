---
title: 'survqual: An R package simulataneous analysis of survival and quality of life data'
output: 
  html_document:
    keep_md: true
tags:
  - R
  - R package
  - package
  - survival
  - survival analysis
  - quality of life
  - Q-TWiST
authors:
  - name: Sebastian Gerdes
    orcid: 0000-0002-9433-5631
    equal-contrib: true
    affiliation: "1" # (Multiple affiliations must be quoted)
    corresponding: true # (This is how to denote the corresponding author)
  - name: Ingo Roeder
    equal-contrib: true # (This is how you can denote equal contributions between multiple authors)
    affiliation: "1, 2, 3, 4"
affiliations:
 - name: Institute for Medical Informatics and Biometry, Carl Gustav Carus Faculty of Medicine, Technische Universität Dresden, Dresden, Germany 
   index: 1
 - name: National Center for Tumor Diseases (NCT), Dresden, Germany
   index: 2
 - name: German Cancer Research Center (DKFZ), Heidelberg, Germany
   index: 3
 - name: Faculty of Medicine and University Hospital Carl Gustav Carus, Technische Universität Dresden, Dresden, Germany
   index: 4
date: "2023-04-14"
bibliography: ../vignettes/survqual.bib

# Optional fields if submitting to a AAS journal too, see this blog post:
# https://blog.joss.theoj.org/2018/12/a-new-collaboration-with-aas-publishing
# aas-doi: 10.3847/xxxxx <- update this with the DOI from AAS once you know it.
# aas-journal: Astrophysical Journal <- The name of the AAS journal.
---

# Summary
Survival times are used in many clinical studies as a primary endpoint. Oftentimes, quality of life measurements are also included. Although mostly reported separately, there are different methods published to combine survival data and quality of life data into a single endpoint. `survqual` is an R package implementing some of the published statistical methods.

# Statement of need
437,713 research studies in clinicialtrials.gov, 



# Summary
``survqual`` is an R package.


* There are different methods published for the simultaneous analysis of survival data and quality of life data
* Simple models to adjust for quality of life by transforming to QALYs
* More complicated models where the survival data and longitudinal data are described by a single likelihood function -> difficult to interpret, computationally complicated and expensive (also sometimes convergence problems)
* In the literature, bias because of informative censoring is identied as a serious drawback of the simple models
* Here, we want to study the magnitude of the bias in a simulation study and show methods how the magnitude of the bias can be estimated / guessed
* We show that in many situations, the bias is not substantial and therefore the simple models can be used to adequately model the data, which is nice because the simple models can easily interpreted in contrast to the more sophisticated models
* Furthermore, we developed an R package providing convenient functions to use the simple models


# Background
* Introduction of Q-TWiST (widely used method for estimating quality adjusted life-time) [@glasziou_quality_1990, @glasziou_quality_1998]
* Reanalysis of several studies using Q-TWiST [@solem_systematic_2018]
* Good review describing a variety of methods that could be partially implemented in an R package [@billingham_simultaneous_2002]

# Motivation
Survival time is a common primary endpoint in clinical trials. However, quality of life may differ systematically between different arms of a clinical trial. Overall, many agree that quality of life should be considered in addition to survival time when comparing different treatments. Different approaches combining survival and quality of life have been described in the literature. 

A very simple model is the following ($t_0$ denoting event time without adjustment, ${t_0}^\prime$ the quality-adjusted event-time, $q(t)$ denoting the quality of life in dependence of time $t$:
$$
{t_0}^\prime = \int_0^{t_0} q(t) dt
$$
# References
