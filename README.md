
- <a href="#r-package-survqual" id="toc-r-package-survqual">R package
  survqual</a>
  - <a href="#background" id="toc-background">Background</a>
  - <a href="#dependencies" id="toc-dependencies">Dependencies</a>
  - <a href="#installation" id="toc-installation">Installation</a>
  - <a href="#more-detailed-information"
    id="toc-more-detailed-information">More detailed information</a>
  - <a href="#feedback" id="toc-feedback">Feedback</a>
  - <a href="#example-of-simple-adjustment-of-survival-times"
    id="toc-example-of-simple-adjustment-of-survival-times">Example of
    simple adjustment of survival times</a>
  - <a href="#references" id="toc-references">References</a>

<!-- README.md is generated from README.Rmd. Please edit that file -->

# R package survqual

The goal of this package is to provide functions for simultaneous
analysis of quality of life data and survival data.

## Background

In most studies evaluating therapeutic strategies in oncology, survival
time is taken as the primary endpoint. In our opinion, quality of life
should in many situations given a greater weight in the comparison of
therapeutic strategies. This can be done by simultaneous analysis of
survival and quality of life. Several methods to combine survival and
quality of life measurements have been described in the literature (see
Billingham and Abrams (2002) for a good review of established methods).

However, in the most widely-used statistical software packages (e. g. R,
SAS, SPSS) methods to carry out simultaneous analysis of survival and
quality of life are not implemented.

Therefore, we here

- develop an open source package for the statistical software R so that
  some of the methods described in the literature can be conveniently
  applied (currently prototype available, needs still testing and
  refinement)
- demonstrate the value of a combined survival and quality of life
  analysis by analyzing available data from in silico data (done) and
  completed and published clinical trials (yet to be done)
- demonstrate also subtleties and pitfalls that arise when combining
  survival and quality of life in a single endpoint (to be done)

$$
QALY(L) = \int_0^{L} Q(t) S(t) dt
$$

$$
\bar{t}_i = \int_0^{t_1} Q_i(t) dt
$$

$$
{Q-TWiST}_i = u_{TOX} TOX_i + u_{TWiST} TWiST_i + u_{REL} REL_i
$$

## Dependencies

The survqual package suggests a few other R packages. Among them are a
few [tidyverse](https://www.tidyverse.org/) packages, however, the
survqual package can be used quite well without tidyverse packages. It
is more a question of convenience of the maintainer that the packages
are listed as suggested in the description file of the survqual package.

## Installation

This R package is hosted [here](https://gitlab.com/imb-dev/survqual) at
[GitLab.com](https://gitlab.com/).

You can install the development version of `survqual` from GitLab with
the following commands:

``` r
install.packages('devtools')
devtools::install_gitlab('imb-dev/survqual', 
                         build_vignettes = TRUE)
```

<!-- To install a specific version, add the version tag after the name, separated by a @, e.g. to install `survqual` in version v0.0.0.902 use -->
<!-- ```{r} -->
<!-- devtools::install_gitlab('imb-dev/survqual@v0.0.0.902') -->
<!-- ``` -->

A [CRAN](https://cran.r-project.org/) version does not exist (yet).

## More detailed information

A more detailed introduction can be found in the package vignette (not
yet).

``` r
vignette(topic = 'survqual', 
         package = 'survqual')
```

## Feedback

Feedback (bug reports and feature requests) is welcome any time. Please
create a gitlab issue (<https://gitlab.com/imb-dev/survqual/-/issues>)
or send a mail to sebastian.gerdes at tu-dresden dot de!

## Example of simple adjustment of survival times

First, we load the required packages:

``` r
library('survival')
library('tidyverse')
library('survqual')
```

We look at a simplistic dataset included in the survqual package
consisting of two dataframes:

- `sq_simple_surv` contains the survival data. The survqual package
  requires the first column to contain a unique patient id, the second
  column the survival time and the third column the survival status.
  Additionally, more columns can contain covariables.
- `sq_simple_qol` contains the quality of life data. The survqual
  package requires that it has only three columns. The first column has
  to contain a unique patient id. The second column contains the time of
  measurement. The third column contains the quality of life at this
  time scaled between 0 and 1.

``` r
head(sq_simple_surv)
#>   id time status group    x1 x2
#> 1  1   76      0     b  0.13 f2
#> 2  2  118      0     a  0.80 f2
#> 3  3   15      0     a -0.06 f2
#> 4  4   14      0     b  0.50 f2
#> 5  5   44      0     a  1.09 f1
#> 6  6  289      1     b -0.69 f2
head(sq_simple_qol)
#>   id time  qol
#> 1  1   25 0.99
#> 2  1   36 0.63
#> 3  1   68 0.21
#> 4  2  102 0.13
#> 5  2   46 0.48
#> 6  2   92 0.92
```

It can be checked with a function if the data are compliant with these
requirements:

``` r
are_data_survqual_compliant(sq_simple_surv, sq_simple_qol)
#> The data seem to be compliant!
#> [1] TRUE
```

For applying the analysis functions, the two data frames have to be
joined:

``` r
expanded_data <- create_expanded_data(sq_simple_surv, sq_simple_qol)
head(expanded_data)
#>   id time_surv status group   x1 x2 time_qol  qol
#> 1  1        76      0     b 0.13 f2       25 0.99
#> 2  1        76      0     b 0.13 f2       36 0.63
#> 3  1        76      0     b 0.13 f2       68 0.21
#> 4  2       118      0     a 0.80 f2      102 0.13
#> 5  2       118      0     a 0.80 f2       46 0.48
#> 6  2       118      0     a 0.80 f2       92 0.92
```

The survqual package provides several methods obtain a continuous
function for the quality of life.

The most simple method is linear interpolation between the measurement
points.

``` r
fm_approx <- create_approx_model(expanded_data)
pred_approx <- create_qol_prediction(expanded_data, fm_approx)
head(pred_approx)
#>     id time_surv status group   x1 x2  time_qol  qol
#> 1    1        76      0     b 0.13 f2 0.0000000 0.99
#> 1.1  1        76      0     b 0.13 f2 0.3819095 0.99
#> 1.2  1        76      0     b 0.13 f2 0.7638191 0.99
#> 1.3  1        76      0     b 0.13 f2 1.1457286 0.99
#> 1.4  1        76      0     b 0.13 f2 1.5276382 0.99
#> 1.5  1        76      0     b 0.13 f2 1.9095477 0.99
ggplot(pred_approx, aes(time_qol, qol, colour = factor(id))) + 
  geom_line() + ylim(0, 1) +
  geom_point(data = expanded_data, aes(x = time_qol, y = qol, colour = factor(id))) +
  labs(colour = 'id') + xlab('Time') + ylab('Quality of Life')
```

<img src="man/figures/README-sq_simple_approx-1.png" width="100%" />

The predicted functions can be used to calculate quality-adjusted
survival times:

``` r
(sq_simple_surv$adjusted_approx <- adjust(pred_approx))
#>  [1] 48.779732 61.608629 12.875007  4.270027 18.110020 99.505736 50.755087
#>  [8] 29.010806 26.254656  4.694982
```

Furthermore, any fitted statistical model to predict quality of life
values that has a suitable `predict` method can be used to obtain a
continuous function the quality of life.

For example, a generalized additive model can be fitted and applied like
this:

``` r
fm_gam <- gam::gam(qol ~ gam::s(time_qol, 1) + group + x1 + x2,
                      data = expanded_data)
pred_gam <- create_qol_prediction(expanded_data, fm_gam)
ggplot(data = pred_gam, aes(x = time_qol, y = qol, colour = factor(id))) + 
  geom_line() + ylim(0, 1) +
  labs(colour = 'id') + xlab('Time') + ylab('Quality of Life')
```

<img src="man/figures/README-sq_simple_gam-1.png" width="100%" />

``` r
(sq_simple_surv$adjusted_gam <- adjust(pred_gam))
#>  [1]  36.571999  71.099771  10.700021   7.174359   8.974375 102.842942
#>  [7]  61.823688  27.039669  46.189546   4.358704
```

Also a linear model to obtain continuous quality of life values can
easily be fitted and used to calculate quality-adjusted survival times:

``` r
fm_lm <- lm(qol ~ time_qol + group + x1 + x2,
                data = expanded_data)
pred_lm <- create_qol_prediction(expanded_data, fitted_model = fm_lm)
ggplot(data = pred_lm, aes(x = time_qol, y = qol, colour = factor(id))) + 
  geom_line() + ylim(0, 1) + 
  labs(colour = 'id') + xlab('Time') + ylab('Quality of Life')
```

<img src="man/figures/README-sq_simple_linear_model-1.png" width="100%" />

``` r
(sq_simple_surv$adjusted_lm <- adjust(pred_lm))
#>  [1]  36.571999  71.099771  10.700021   7.174359   8.974375 102.842942
#>  [7]  61.823688  27.039669  46.189546   4.358704
```

Also more complex models like mixed models can be applied:

``` r
fm_lmer <- lme4::lmer(qol ~ (1 | id) + time_qol + group + x1 + x2,
                data = expanded_data)
pred_lmer <- create_qol_prediction(expanded_data, fitted_model = fm_lmer)
ggplot(data = pred_lmer, aes(x = time_qol, y = qol, colour = factor(id))) + 
  geom_line() + 
  ylim(0, 1) + xlab('Time') + ylab('Quality of Life') + labs(colour = 'id')
```

<img src="man/figures/README-sq_simple_mixed_model-1.png" width="100%" />

``` r
(sq_simple_surv$adjusted_lmer <- adjust(pred_lmer))
#>  [1]  37.314444  70.553110  10.776937   6.985948   9.155158 106.634672
#>  [7]  60.798905  27.377713  45.373561   4.294564
```

Oftentimes, the different methods yield quite similar results. The
following plot contrasts the different methods.

``` r
sq_simple_surv |> 
  mutate(not_adjusted = time) |> 
  pivot_longer(cols = c('not_adjusted', 'adjusted_approx', 'adjusted_gam', 'adjusted_lm', 'adjusted_lmer')) |> 
  ggplot(aes((id), value, colour = name)) + 
  geom_point(size = 2) + geom_line() +
  labs(x = 'Patient ID', y = 'Time', colour = 'Method') +
  scale_y_continuous(trans = 'log10')
```

<img src="man/figures/README-sq_simple_comparison-1.png" width="100%" />

## References

<div id="refs" class="references csl-bib-body hanging-indent">

<div id="ref-billingham_simultaneous_2002" class="csl-entry">

Billingham, L. J., and K. R. Abrams. 2002. “Simultaneous Analysis of
Quality of Life and Survival Data.” *Statistical Methods in Medical
Research* 11 (1): 25–48. <https://doi.org/10.1191/0962280202sm269ra>.

</div>

</div>
